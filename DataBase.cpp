#include "DataBase.h"

#include <string>
#include "Helper.h"
using namespace std;
DataBase::DataBase()
{
	int rc;
	
	char *zErrMsg = 0;
	bool flag = true;

	rc = sqlite3_open("trivia.db", &_db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
	}

	system("CLS");

}
DataBase::~DataBase()
{
	sqlite3_close(_db);
	_db = NULL;

}

bool DataBase::isUserExists(string username)
{
	string sql;
	int ans=0;
	char* zErrMsg=0;
	sql = "select count(*) from t_users where username = \'" + username+"\'";
	
	int rc = sqlite3_exec(_db, sql.c_str(), callbackCount, &ans, &zErrMsg);
	if(rc != SQLITE_OK)
	{
		cout << "ERROR!! isUserExists faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
	}

	return ans != 0;
}
bool DataBase::addNewUser(string _username, string _password, string _email)
{
	string sql;
	
	char* zErrMsg = 0;
	sql = "insert into t_users  (username, password, email) values (\'" + _username + "\',\'" + _password + "\',\'" + _email + "\')";

	int rc = sqlite3_exec(_db, sql.c_str(), nullptr, nullptr, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! isUserExists faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(string name, string password)
{

	string sql;
	int ans = 0;

	char* zErrMsg = 0;
	sql = "select count(*) from t_users where username = \'" + name + "\'and password = \'"+password+ "\'";

	int rc = sqlite3_exec(_db, sql.c_str(), callbackCount, &ans, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! isUserExists faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	return ans != 0;

}
std::vector<Question*> DataBase::initQuestions(int questNumber)
{
	string sql = "select question_id ,question,correct_ans ,ans2 ,	ans3 ,ans from t_users by random() limit\' " + to_string(questNumber);
	char* zErrMsg = 0;
	vector<Question*> qust;


	int rc = sqlite3_exec(_db, sql.c_str(), callbackQuestion , 0 , &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! initQuestions faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
	}
	return qust;

}
int DataBase::insertNewGame()
{
	string sql = "insert into t_game (status,start_time ) valuse (0,DATETIME(\'now\'))";
	char* zErrMsg = 0;
	int ans;
	int rc = sqlite3_exec(_db, sql.c_str(), NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! insertNewGame faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	sql = "select last_insert_rowid()";

	rc = sqlite3_exec(_db, sql.c_str(), callbackCount, &ans, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! insertNewGame faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	return ans;
}

bool DataBase::updateGameStatus(int gameID)
{

	string sql = "UPDATE people SET status = 1, end_time = DATETIME(\'now\') WHERE game_id =\'" + to_string(gameID);
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, sql.c_str(), NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! updateGameStatus faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	string sql = "insert into t_players_answers (gameId,username,questionId,answer,isCorrect,answerTime) valuse(\'" + to_string(gameId) + "\\',\'" + username + "\\',\'" + to_string(questionId) + "\\',\'" + answer + "\\',\'" + to_string(isCorrect) + "\\',\'" + to_string(answerTime) + "\')";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, sql.c_str(), NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "ERROR!! updateGameStatus faild ErrMsg =" << zErrMsg << "sql %s=" << sql << endl;
		return false;
	}
	return true;
}



	std::vector<string> DataBase::getBestScores()
	{
		vector<string> score;
		char* zErrMsg = 0;
		int rc;
		string sqlS = "select username, sum(is_correct) as ans from t_players_answers group by username order by ans DESC linit 3";
		rc = sqlite3_exec(_db, sqlS.c_str(), callbackBestScores, &score, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		return score;
	}

	std::vector<string> DataBase::getPersonalStatus(string userName)
	{
		vector<string> stats;
		char* zErrMsg = 0;
		int rc;
		string sqlS = "select count(*), sum(is_correct), count(*) - sum(is_correct) from t_players_answers where username = \'" + userName + "\'";
		rc = sqlite3_exec(_db, sqlS.c_str(), callbackBestScores, &stats, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		return stats;
	}



static int callbackCount(void* notUsed, int argc, char** argv, char** azCol)
{
	int* res = (int*)notUsed;
	*res = atoi(argv[0]);
	return 0;
}

static int callbackQuestion(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<Question*>* res = (vector<Question*>*)notUsed;
	Question* currQuest = new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]);
	res->push_back(currQuest);
	return 0;
}

static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<string>* res = (vector<string>*)notUsed;
	string username = argv[0];
	int count = atoi(argv[0]);
	string s = Helper::getPaddedNumber(username.length(), 2) + username + Helper::getPaddedNumber(count, 6);
	res->push_back(s);
	return 0;
}

static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)

{
	vector<string>* res = (vector<string>*)notUsed;
	string s1 = argv[0];
	if (s1 == "0")
	{
		res->push_back("0");
		res->push_back("0");
		res->push_back("0");
		res->push_back("0");
	}
	else
	{
		res->push_back(argv[0]);
		res->push_back(argv[1]);
		res->push_back(argv[2]);
		string avg = argv[3];
		char buffer[50];
		printf(buffer, "%.2f", atof(avg.c_str()));
		int i = atoi(buffer);
		string s = "";
		if (i < 10)
		{
			s = "0";
			s += buffer[0];
			s += buffer[1];
			s += buffer[2];
		}
		else
		{
			s = buffer[0];
			s += buffer[1];
			s += buffer[3];
			s += buffer[4];
		}
		res->push_back(s);
	}
	return 0;


}