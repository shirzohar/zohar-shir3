#include "TriviaServer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>       
#include <thread>
#include <vector>
#include <mutex>          // std::mutex, std::unique_lock, std::defer_lock

std::mutex mtx;           // mutex for critical section


using namespace std;
TriviaServer::TriviaServer()
{
	/* The port number is passed as an argument */

	try
	{
		// create a socket
		// socket(int domain, int type, int protocol)
		_socket = socket(AF_INET, SOCK_STREAM, 0);
		if (_socket < 0)
			throw "ERROR opening socket";
	}
	catch (char* msg)
	{
		perror(msg);
	}
}
TriviaServer::~TriviaServer()
{
	std::map<SOCKET, User*>::iterator it;
	std::map<int, Room*>::iterator it2;
	closesocket(TriviaServer::_socket);
	for (it2 == _roomsList.begin(); it2 != _roomsList.end(); ++it2)
	{
		_roomsList.erase(it2);
	}
	for (it == _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		_connectedUsers.erase(it);
	}
}

void TriviaServer::serve()
{

	bindAndListen();
	thread Thread(&TriviaServer::clientHandler, this);
	Thread.detach();
	while (true)
	{
		TRACE("sccepting client");
		try
		{
			accept();
		}
		catch (...)
		{

		}
	}



}

void TriviaServer::bindAndListen()
{
	int iResult;
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = 0;
	clientService.sin_port = htons(27015);
	try
	{
		iResult = ::bind(TriviaServer::_socket, (SOCKADDR *)& clientService, sizeof(clientService));
		if (iResult == SOCKET_ERROR)
		{
			throw (L"bind function failed with error: %ld\n", WSAGetLastError());
		}
	}
	catch (char* msg)
	{
		cout << msg << endl;
	}
	try
	{
		if (::listen(TriviaServer::_socket, SOMAXCONN) == SOCKET_ERROR)
			throw (L"listen function failed with error: %d\n", WSAGetLastError());
		cout << (L"Listening on socket...\n") << endl;
	}
	catch (char* msg)
	{
		cout << msg << endl;
	}
}

void TriviaServer::accept()
{
	SOCKET socket_client = ::accept(TriviaServer::_socket, NULL, NULL);
	if (socket_client == INVALID_SOCKET)
	{
		throw("ERROR!invalid socket!!");
	}
	thread accept_thread(&TriviaServer::_socket, this, socket_client);
	accept_thread.detach();
}
void TriviaServer::clientHandler(SOCKET socket_client)
{

	RecievedMessage* rm;
	int Bmessage;
	do
	{
		Bmessage = Helper::getMessageTypeCode(socket_client);
		string message = to_string(Bmessage);
		int length = message.length();
		if ((message[length - 1] != 0) || (message[0] != 299))
		{
			rm = buildRecieveMessage(socket_client, Bmessage);
			addRecievedMessage(rm);
		}
	} while (Bmessage = !0);
	RecievedMessage* quit;
	quit = buildRecieveMessage(socket_client, QUIT_APP);
	_queRcvMessages.push(quit);

}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	if (msgCode == ASK_SIGN_IN)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string passWord = Helper::getStringPartFromSocket(client_socket, passSize);
		values.push_back(userName);
		values.push_back(passWord);

	}
	else if (msgCode == ASK_SIGN_UP)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string passWord = Helper::getStringPartFromSocket(client_socket, passSize);
		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);
		values.push_back(userName);
		values.push_back(passWord);
		values.push_back(email);
	}
	else if (msgCode == ASK_CREATE_ROOM)
	{
		int roomNameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string roomName = Helper::getStringPartFromSocket(client_socket, roomNameSize);
		string playersNo = Helper::getStringPartFromSocket(client_socket, 1);
		string QuestionsNo = Helper::getStringPartFromSocket(client_socket, 2);
		string AnswerTime = Helper::getStringPartFromSocket(client_socket, 2);
		values.push_back(roomName);
		values.push_back(playersNo);
		values.push_back(QuestionsNo);
		values.push_back(AnswerTime);
	}
	else if (msgCode == ASK_CLOSE_ROOM || msgCode == ASK_LEAVE_ROOM || msgCode == QUIT_APP || msgCode == ASK_ROOM_LIST)
	{
		// do nothing becuase there arent another data in the recieved message (according protocol)
	}
	else if (msgCode == ASK_ROOM_USERS || msgCode == ASK_JOIN_ROOM)
	{
		string roomId = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomId);
	}
	else if (msgCode == C_SEND_ANS)
	{
		string answerNo = Helper::getStringPartFromSocket(client_socket, 1);
		string answerTime = Helper::getStringPartFromSocket(client_socket, 2);

		values.push_back(answerNo);
		values.push_back(answerTime);

	}

	msg = new RecievedMessage(client_socket, msgCode, values);
	return msg;
}
Room* TriviaServer::getRoomById(int roomId)
{
	Room* room;
	auto search = _roomsList.find(roomId);
	if (search != _roomsList.end()) {
		room = search->second;
	}
	else {
		room = nullptr;
	}

	return room;
}
User* TriviaServer::getUserByName(string username)
{
	User* user;
	User* GoodUser;
	std::map<SOCKET, User*>::iterator it_type;
	for (it_type = _connectedUsers.begin(); it_type != _connectedUsers.end(); it_type++){
		user = it_type->second;
		if ((user->getUsername()).compare(username) == 0)
		{
			GoodUser = user;
		}
		else
		{
			GoodUser = nullptr;
		}
	}

	return GoodUser;
}

User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	User* user;
	User* GoodUser;
	std::map<SOCKET, User*>::iterator it_type;
	for (it_type = _connectedUsers.begin(); it_type != _connectedUsers.end(); it_type++){
		user = it_type->second;
		if (user->getSocket() == client_socket)
		{
			GoodUser = user;
		}
		else
		{
			GoodUser = nullptr;
		}
	}

	return GoodUser;
}
void TriviaServer::handleRecievedMessages()
{
	RecievedMessage* chargeMessage;
	SOCKET userSock;
	User* theUser;
	int message;
	vector <string> messageValuse;
	unique_lock<mutex> lck(mtx, defer_lock);
	if (_queRcvMessages.empty())
	{
		_CV.wait(lck);
		chargeMessage = _queRcvMessages.back();
		_CV.notify_all();
		userSock = chargeMessage->getSock();
		theUser = getUserBySocket(userSock);//get the user
		message = chargeMessage->getMessageCode();//get the code
		if (message == ASK_SIGN_IN)
		{
			theUser = handleSignin(chargeMessage);

		}

		else if (message == ASK_SIGN_OUT)
		{
			handleSignout(chargeMessage);
		}

		else if (message == ASK_SIGN_UP)
		{
			bool succeses = handleSignup(chargeMessage);
		}


		else if (message == ASK_ROOM_LIST)
		{
			handleGetRoom(chargeMessage);
		}


		else if (message == ASK_ROOM_USERS)
		{
			handleGetUsersInRoom(chargeMessage);
		}


		else if (message == ASK_JOIN_ROOM)
		{
			bool succeses = handleJoinRoom(chargeMessage);
		}



		else if (message == ASK_CREATE_ROOM)
		{
			bool succeses = handleCreateRoom(chargeMessage);
		}
		else if (message == ASK_CLOSE_ROOM)
		{

			bool succeses = handleCloseRoom(chargeMessage);
		}

		else if (message == ASK_START_GAME)
		{
			handleStartGame(chargeMessage);
		}

		else if (message == C_SEND_ANS)
		{
			handlePlayerAnswer(chargeMessage);
		}
		else if (message == LEAVE_GAME)
		{
			handleLeaveGame(chargeMessage);
		}

		else if (message == ASK_BEST_SCORES)
		{
			handleGetBestScores(chargeMessage);
		}

		else if (message == ASK_PERSONSL_SITUATION)
		{
			handleGetPersonalStatus(chargeMessage);
		}
		else if (message == QUIT_APP)
		{
			safeDeleteUser(chargeMessage);
		}
		else
		{
			safeDeleteUser(chargeMessage);
		}

		_queRcvMessages.pop();
	}


}
void TriviaServer::safeDeleteUesr(RecievedMessage* _RM)
{
	try
	{
		SOCKET clientSock;
		clientSock = _RM->getSock();
		handleSignout(_RM);
		closesocket(clientSock);
	}
	catch (...)
	{
		cout << "ERROR!! in function: safeDeleteUesr" << endl;
	}

}

User* TriviaServer::handleSignin(RecievedMessage* _RM)
{
	RecievedMessage* RmToSend;
	vector <string> values;
	values = _RM->getValues();
	User* theuser;
	string user;
	string password;
	user = values.front();
	password = values.back();
	bool exists;
	SOCKET userSock = _RM->getSock();
	exists = _DB->isUserAndPassMatch(user, password);//check if the user exists
	if (exists == false)
	{
		Helper::sendData(userSock, SIGHN_WRONG_DETAILS);
		return NULL;
	}
	theuser = getUserByName(user);
	if (theuser == NULL)
	{
		Helper::sendData(userSock, SIGHN_USER_ALREDY_CONNECT);
		return NULL;
	}
	theuser = new User(user, userSock);
	pair<SOCKET, User*> p(userSock, theuser);
	_connectedUsers.insert(p);
	theuser->Send(SIGHN_SUCCESE);
	return theuser;
}

void TriviaServer::handleSignout(RecievedMessage* _RM)
{
	User* toErase = _RM->getUser();
	if (toErase != nullptr)
	{
		SOCKET sockToEras = _RM->getSock();
		_connectedUsers.erase(sockToEras);//erase the user that want to disconnect
	}
	handleLeaveGame(_RM);
	bool work = handleLeaveRoom(_RM);
	if (work == true)
	{
		bool succses = handleCloseRoom(_RM);
		if (succses == true)
		{

		}
		else{ cout << "error when try to close room." << endl; }
	}
	else{ cout << "error when try to leave room." << endl; }
}

void handleLeaveGame(RecievedMessage* _RM)
{
	User* theUser = _RM->getUser();
	Game* theGame = theUser->getGame();
	bool work = theUser->leaveGame();
	if (work == true)//the game had finish
	{
		delete theGame;
	}

}
bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	string name, pass, email;
	vector<string> UserValues;
	name = UserValues[0];
	pass = UserValues[1];
	email = UserValues[2];

	bool check = _DB->isUserExists(name);
	if (check)
	{
		bool success = _DB->addNewUser(name, pass, email);
		if (success)
		{
			cout << "user signup!" << endl;
		}
		else
		{
			cout << "user  isn't signup!" << endl;
		}
	}
	else
	{
		cout << "ERROR!! user  name alredy exist" << endl;
	}
	


}

void TriviaServer::handleStartGame(RecievedMessage* _RM)
{
	Game* gm = nullptr;
	Room* usersRoom;
	try
	{
		User* theUser = _RM->getUser();
		usersRoom = theUser->getRoom();
		gm = new Game (usersRoom->getUsers(), usersRoom->getQuestionsNo(), *(this->_DB) , usersRoom->getld());

	}
	catch (...)
	{
		cout << "ERROR!! when starting game: handleStartGame " << endl;
	}
	int RoomId = usersRoom->getld();
	_roomsList.erase(RoomId);
	gm->sendFirstQuestion();
	cout << "game started " << endl;
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	Game* gm = us->getGame();
	vector<string> values;
	std::string::size_type sz;
	values = _RM->getValues();
	if (gm != nullptr)
	{
		gm->handleAnswerFromUser(us, std::stoi(values.at(0), &sz), std::stoi(values.at(1), &sz));///���

	}
	else
	{
		handleLeaveGame(_RM);
		cout << "game finished and was deleted from memory " << endl;
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	bool good;
	vector<string> values;
	std::string::size_type sz;
	if (us != nullptr)
	{
		_roomIdSequence += 1;
		good = us->createRoom(_roomIdSequence, values.at(0), std::stoi(values.at(1), &sz), stoi(values.at(2), &sz), stoi(values.at(3), &sz));///���
		if (good)
		{
			_roomsList.insert(std::pair<int, Room*>(_roomIdSequence, us->getRoom()));

			cout << "room had create " << endl;

			return true;

		}
		else{return false;}
	}
	else{ return false; }
}

bool TriviaServer::handleCloseRoom(RecievedMessage* _RM)
{
	Room* rm = _RM->getUser()->getRoom();
	int check;
	if (rm)
	{
		check =_RM->getUser()->closeRoom();
		if (check != (-1))
		{
			_roomsList.erase(rm->getld());
			cout << "room had close " << endl;

			return true;
		}
		else{ return false; }

	}
	else{ return false; }

}

bool TriviaServer::handleJoinRoom(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	vector<string> values;
	std::string::size_type sz;	if (us)
	{
		Room* rm = getRoomById(stoi(values.at(0),&sz));
		if (rm)
		{
			us->joinRoom(rm);
			return true;
		}
		else
		{
			Helper::sendData(us->getSocket(), RSP_JOIN_ROOM_NOT_EXIST);
			return false;

		}
	}
	else{ return false; }
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	//int check;
	if (us)
	{
		Room* rm = us->getRoom();
		if (rm)
		{
			us->leaveRoom();
			return true;
		}
		else{ return false; }

	}
	else{ return false; }
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	vector<string> values;
	std::string::size_type sz;	if (us)
	{
		Room* rm = getRoomById(stoi(values.at(0), &sz));
		if (rm)
		{
			string message = rm->getUsersListMessage();
			Helper::sendData(us->getSocket(), message);
		}
		else
		{
			Helper::sendData(us->getSocket(), RSP_JOIN_ROOM_NOT_EXIST);
			
		}
	}
	
}

void TriviaServer::handleGetRoom(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	int size = _roomsList.size();
	string message = RSP_ROOM_LIST+size;
	if (us)
	{
		
		for (auto& x : _roomsList) 
		{
			message = message + to_string(x.second->getld()) + to_string((x.second->getName()).length()) + x.second->getName();
		}

		us->Send(message);
	}
}

void TriviaServer::handleGetBestScores(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	string message;

	vector<string> BScores;
	BScores = _DB->getBestScores();

	if (BScores.size() >= 3)
	{

		message = to_string(RSP_BEST_SCORES) + BScores.at(0) + BScores.at(1) + BScores.at(2);
	}
	else if (BScores.size() == 2)
	{
		message = to_string(RSP_BEST_SCORES) + BScores.at(0) + BScores.at(1) + "00";

	}
	else if (BScores.size() == 1)
	{
		message = to_string(RSP_BEST_SCORES) + BScores.at(0) + "00" + "00" ;

	}
	else
	{
		message = to_string(RSP_BEST_SCORES) + "00" + "00" + "00";

	}
	
	us->Send(message);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* _RM)
{
	User* us = _RM->getUser();
	string message;
	vector<string> Pstatus = _DB->getPersonalStatus(us->getUsername());
	int games = stoi(Pstatus.at(0));
	int correct = stoi(Pstatus.at(1));
	int wrong = stoi(Pstatus.at(2));
	message = to_string(RSP_PERSONSL_SITUATION) + Helper::getPaddedNumber(games, 4) + Helper::getPaddedNumber(correct, 6) + Helper::getPaddedNumber(wrong, 6) + Pstatus.at(3);
	us->Send(message);
}
void TriviaServer::addRecievedMessage(RecievedMessage* _RM)
{
	unique_lock<mutex> lck(mtx, defer_lock);
	_CV.wait(lck);
	_queRcvMessages.push(_RM);
	_CV.notify_all();
}
