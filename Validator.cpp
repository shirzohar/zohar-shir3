#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	/*
	Functionality: Checks whether a password is valid. These are the rules:
	- Must contain at least 4 characters
	- Can not contain spaces
	- Must contain at least one digit
	- Must contain at least one upper case letter
	- Must contain at least one lower case letter
	*/
	int numPass = 0;
	int upperCasePass = 0;
	int lowerCasePass = 0;
	if (password.length() < 4)
		return false;
	for (int i = 0; i < password.length(); i++)
	{
		if (password[i] == ' ')
			return false;
		else if ((int)password[i] >= 48 && (int)password[i] <= 57)
			numPass++;
		else if ((int)password[i] >= 65 && (int)password[i] <= 90)
			upperCasePass++;
		else if ((int)password[i] >= 97 && (int)password[i] <= 122)
			upperCasePass++;
	}
	if (numPass == 0 || upperCasePass == 0 || lowerCasePass == 0)
		return false;
	return true;
}
bool Validator::isUsernameValid(string username)
{
	/*
	Functionality: Checks whether a name is entered valid. These are the rules:
	- Must begin with the letter
	- Can not contain spaces
	- can not be empty
	*/
	int startWithLatter = 0;
	if (((int)username[0] >= 65 && (int)username[0] <= 90) || ((int)username[0] >= 97 && (int)username[0] <= 122));
	else
	{
		return false;
	}
	for (int i = 0; i < username.length(); i++)
	{
		if (username[i] == ' ')
			return false;
	}
	if (username.length() == 4)
		return false;
	return true;
}