#pragma once
#include <vector>
#include <string>
#include "User.h"
#include "Windows.h"
#include"Protocol.h"
using namespace std;
class User;

class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNO, int questionTime);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector <User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getld();
	string getName();

private:
	string getUsersAsString(vector<User*> usersList, User* excludeUser);
	void sendMessage(string message);
	void sendMessage(User* excludeUser, string message);
	vector<User*> _users; //� Users connected to the room.
	User* _admin; //� The users opened the room
	int _maxUsers; // � Maximum number of users per room
	int _questionTime; // � Maximum response time to question
	int _questionsNo; // � Number of max questions in the game
	string _name; //� Room name
	int _id; //� room id
};