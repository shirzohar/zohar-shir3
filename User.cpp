#include "User.h"

#include <string>
#include <iostream>
using namespace std;

User::User(string username, SOCKET socket)
{
	_username = username;
	_sock = socket;
}
string User::getUsername()
{
	return _username;
}
SOCKET User::getSocket()
{
	return _sock;
}
Room* User::getRoom()
{
	return _currRoom;
}
Game* User::getGame()
{
	return _currGame;
}
void User::Send(string message)
{
	Helper::sendData(_sock, message);
	cout << "Message sent to user:" << _username.c_str() << " msg:" << message.c_str() << endl;
}
void User::setGame(Game* gm)
{
	_currRoom = NULL;
	_currGame = gm;
}

void User::clearGame()
{
	_currGame = NULL;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != NULL)
	{
		Send(RSP_CREATE_ROOM_FAILED);
		return false;
	}
	else
	{
		Room* new_room = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		_currRoom = new_room;
		Send(RSP_CREATE_ROOM_SUCCESE);
		return true;
	}
}

bool User::joinRoom(Room* newRoom)
{
	if (_currRoom != NULL)
		return false;
	bool a = newRoom->joinRoom(this);
	if (a)
		_currRoom = newRoom;
	return (a);
}

void User::leaveRoom()
{
	if (_currRoom != NULL)
	{
		_currRoom->leaveRoom(this);
		_currRoom = NULL;
	}
}
int User::closeRoom()
{
	if (_currRoom == NULL)
	{
		return -1;
	}
	else
	{
		_currRoom->closeRoom(this);
		int roomNum = _currRoom->getld();
		delete _currRoom;
		return roomNum;
	}
}
bool User::leaveGame()
{
	if (_currGame != NULL)
	{
		bool a = _currGame->leaveGame(this);
		_currGame = NULL;
		return a;
	}
	return false;
}
void User::clearRoom()
{
	_currRoom = NULL;
}