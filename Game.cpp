#include "Game.h"
#include <exception>
#include <sstream>

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db, int id) :_db(db), _questions_no(questionsNo), _players(players), _id(id)
{
	int roomID = _db.insertNewGame();
	if (roomID)
	{
		_questions = _db.initQuestions(questionsNo);
		for each(User* player in _players)
		{
			_results[player->getUsername()] = 0;
			player->setGame(this);
		}
	}
	else
	{
		throw exception("error");
	}
}
Game::~Game()
{
	for each (Question* qust in _questions)
	{
		delete qust;
	}
	for each (User* player in _players)
	{
		delete player;
	}
}
void Game::sendFirstQuestion()
{
	this->sendQustionToAllUsers();
}
void Game::handleFinishGame()
{
	_db.updateGameStatus(_id);
	vector<User*>::iterator ite;
	for (ite = _players.begin(); ite != _players.end(); ite++)
	{

	}
}
bool Game::handleNextTurn()
{
	if (_players.empty())
	{
		handleFinishGame();
		return false;
	}

	if (_currentTurnAswers == _players.size())
	{
		if (_questions.size() == _currQuestionIndex)
		{
			handleFinishGame();
			return false;
		}
		_currQuestionIndex++;
		this->sendQustionToAllUsers();
		return true;
	}
	return true;
}
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool a = true;
	_currentTurnAswers++;

	return a; // ����!!!!!!!!!!
}
bool Game::leaveGame(User* currUser)
{
	bool flag = false;
	vector<User*>::iterator ite;
	for (ite = _players.begin(); ite != _players.end(); ite++)
	{
		if (*ite == currUser)
		{
			flag = true;
		}
	}
	if (flag)
	{
		--ite;
		bool play = handleNextTurn();
		return play;
	}
	return true;
}
int Game::getID()
{
	return _id;
}

bool Game::insertGameToDB()
{

	if (_db.insertNewGame())
	{
		return false;
	}

	return true; // ����!!!!!!!!!!
}
void Game::initQustionFromDb()
{
	_questions = _db.initQuestions(_questions_no);
}
void Game::sendQustionToAllUsers()
{
	stringstream msg;
	try
	{
		msg << "1181";
		string q = _questions[_currQuestionIndex]->getQuestion();
		msg << Helper::getPaddedNumber(q.size(), 3) << q;
		string* answer = _questions[_currQuestionIndex]->getAnswers();
		for (int i = 0; i < 4; i++)
		{
			msg << Helper::getPaddedNumber(answer[i].size(), 3) << answer[i];
		}
	}
	catch (...)
	{
		msg << "1180";
	}
}