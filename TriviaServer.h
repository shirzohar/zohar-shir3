#include "Helper.h"
#include "Room.h"
#include "Game.h"
#include "RecievedMessage.h"
#include "Protocol.h"
#include "DataBase.h"

#include <string>
#include <iostream>
#include <map>
#include <queue>
#include <condition_variable>

using namespace std;
class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();

private:
	std::map<SOCKET, User*> _connectedUsers;//� ����� �� ����� ������� ��������.���� ��� ���� �����.
	std::map<int, Room*> _roomsList;//� ����� �� ����� ������ �������.���� ��� ���� ��� ����.
	queue<RecievedMessage*> _queRcvMessages;// � ��� �� ������ ������ ��������� ��� ���� ���.
	static int _roomIdSequence;
	SOCKET _socket;
	DataBase* _DB;
	std::condition_variable _CV;

	void bindAndListen();
	void accept();
	void addRecievedMessage(RecievedMessage* _RM);
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(RecievedMessage* _RM);
	void handleGetPersonalStatus(RecievedMessage* _RM);
	void handleGetBestScores(RecievedMessage* _RM);
	void handleLeaveGame(RecievedMessage* _RM);
	bool handleCloseRoom(RecievedMessage* _RM);
	void handleStartGame(RecievedMessage* _RM);
	void handlePlayerAnswer(RecievedMessage* _RM);
	void handleGetUsersInRoom(RecievedMessage* _RM);
	bool handleCreateRoom(RecievedMessage* _RM);
	bool handleJoinRoom(RecievedMessage* _RM);
	void handleGetRoom(RecievedMessage* _RM);
	void safeDeleteUesr(RecievedMessage* _RM);
	User* handleSignin(RecievedMessage* _RM);
	bool handleSignup(RecievedMessage* _RM);
	void handleSignout(RecievedMessage* _RM);
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int Bmessage);
	Room* getRoomById(int roomId);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);
	void handleRecievedMessages();
	bool handleLeaveRoom(RecievedMessage* _RM);

};