#include "Room.h"

#include <string>
#include <iostream>

using namespace std;

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNO, int questionTime)
{
	id = _id;
	admin = _admin;
	name = _name;
	maxUsers = _maxUsers;
	questionsNO = _questionsNo;
	questionTime = _questionTime;
	_users.push_back(_admin);
}
string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string users;
	User* u = NULL;
	vector<User*>::iterator ite;
	for (ite = _users.begin(); ite != _users.end(); ite++)
	{
		u = *ite;
		if (u != excludeUser)
			users.append(u->getUsername());
	}
	return users;
}
void Room::sendMessage(string message)
{
	sendMessage(NULL, message);

}
void Room::sendMessage(User* excludeUser, string message)
{
	string name;
	string nameToComper;

	try
	{
		for (int i = 0; i < _users.size(); i++)
		{
			name = _users[i]->getUsername();
			nameToComper = excludeUser->getUsername();
			if (name.compare(nameToComper) != 0)
			{
				excludeUser->Send(message);
			}
		}
	}
	catch (...)
	{
		cout << "An exception occurred.\n";
	}
}
bool Room::joinRoom(User* user)
{
	if (_users.size() == _maxUsers)
	{
		sendMessage(RSP_JOIN_ROOM_FULL);
		return false;
	}
	_users.push_back(user);
	string massage = RSP_JOIN_ROOM_SUCCESE + this->_questionsNo + this->_questionTime;
	sendMessage(massage);
	string usersListMessag = getUsersListMessage();
	sendMessage(user, usersListMessag);
	return true;
}
void Room::leaveRoom(User* user)
{
	bool flag = false;
	vector<User*>::iterator ite;
	for (ite = _users.begin(); ite != _users.end(); ite++)
	{
		if (*ite == user)
		{
			flag = true;
		}
	}
	if (flag)
	{
		--ite;
		_users.erase(ite);
		user->Send(RSP_LEAVE_ROOM_SUCCESE);
		sendMessage(getUsersListMessage());
	}
}

int Room::closeRoom(User* user)
{
	if (user != _admin)
	{
		return -1;
	}
	sendMessage(RSP_CLOSE_ROOM);
	for (int i = 0; i < _users.size(); i++)
	{
		if (user != _admin)
		{
			_users[i]->clearGame();
		}
	}
	_users.clear();
	return _id;
}
vector <User*> Room::getUsers()
{
	return _users;
}
string Room::getUsersListMessage()
{
	string massage = "";
	string name;
	User* temp;
	vector<User*>::iterator ite;
	for (ite = _users.begin(); ite != _users.end(); ite++)
	{
		temp = *ite;
		name = temp->getUsername();
		massage = massage + Helper::getPaddedNumber(name.size(), 2) + name;
	}
	string massage2 = RSP_ROOM_USERS + Helper::getPaddedNumber(_users.size(), 1) + massage;
	return massage2;
}
int Room::getQuestionsNo()
{
	return _questionsNo;
}
int Room::getld()
{
	return _id;
}
string Room::getName()
{
	return _name;
}