#pragma once
#include <iostream>
#include <string>
#include "Question.h"
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
class Game;
class Question;
class DataBase
{
public:

	DataBase();
	~DataBase();

	bool isUserAndPassMatch(string name, string password);
	bool isUserExists(string username);
	bool addNewUser(string _username, string _password, string _email);
	std::vector<Question*> initQuestions(int questNumber);
	std::vector<string>getBestScores();//
	std::vector<string> getPersonalStatus(string userName);//
	int insertNewGame();
	bool updateGameStatus(int gameID);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
	
private:
	sqlite3* _db;

	static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackQuestion(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);

};
