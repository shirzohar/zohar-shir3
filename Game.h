#pragma once
#include <vector>
#include <string>
#include "User.h"
#include "Protocol.h"
#include "DataBase.h"
#include "Question.h"
#include "User.h"
#include <map>
using namespace std;
class Question;
class DataBase;
class User;
class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db, int id);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
	int getID();

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAswers;
	int _id;

	bool insertGameToDB();
	void initQustionFromDb();
	void sendQustionToAllUsers();
};