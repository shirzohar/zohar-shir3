#pragma once
#include <vector>
#include <string>
#include "User.h"
#include <WinSock2.h>
using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode)
	{
		_sock = sock;
		_messageCode = messageCode;
		_user = NULL;
	}
	RecievedMessage(SOCKET sock, int messageCode, vector<string> values) :RecievedMessage(sock, messageCode)
	{
		_values = values;
	}
	SOCKET getSock()
	{
		return _sock;
	}
	User* getUser()
	{
		return _user;
	}
	void setUser(User* user)
	{
		_user = user;
	}
	int getMessageCode()
	{
		return _messageCode;
	}
	vector<string>& getValues()
	{
		return _values;
	}

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};
