#include "Question.h" 

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int r;
	int i = 0;
	bool therIs[4] = { false, false, false, false };
	_id = id;
	_question = question;
	while (i < 4)
	{
		r = rand() % 4;
		if (r == 1 && therIs[r] == false)
		{
			_answers[i] = correctAnswer;
			therIs[r] = true;
			i++;
		}
		else if (r == 2 && therIs[r] == false)
		{
			_answers[i] = correctAnswer;
			therIs[r] = true;
			i++;
		}
		else if (r == 3 && therIs[r] == false)
		{
			_answers[i] = correctAnswer;
			therIs[r] = true;
			i++;
		}
		else
		{
			_answers[i] = correctAnswer;
			therIs[r] = true;
			i++;
		}

		if (i == 0)
		{
			_correctAnswerIndex = r;
		}
	}
}
string Question::getQuestion()
{
	return _question;
}
string* Question::getAnswers()
{
	return _answers;
}
int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}
int Question::getId()
{
	return _id;
}