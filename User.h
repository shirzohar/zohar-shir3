#pragma once
#include "Helper.h"
#include "Room.h"
#include "Game.h"
#include "Windows.h"
#include <string>
#include <iostream>
using namespace std;
class Room;
class Game;
class User
{
public:
	User(string username, SOCKET socket);
	void Send(string message);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);
	void clearRoom();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	void clearGame();
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:

	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};