#include "Helper.h"
#include <string>
#include <iostream>
using namespace std;
class Validator
{
public:
	static bool isPasswordValid(string password);
	static bool isUsernameValid(string username);
};